<?php


namespace App\Controller;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * @Route("Welcome/")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class FrontController extends AbstractController
{
    /**
     * @Route("{chemin}", name="index")
     */

    //Le système de routing interdit l'utilisation du caractère '/' dans le chemin.
    //Ici, il est remplacé par un '-' pour les transpositions


    public function index(Request $request, $chemin = "-")
    {

        //Vérifie la présence d'un caractère "-" au début du chemin, et le retire le cas échéant : évite des éléments orphelins dans le fil d'ariane

        if ($chemin[0] === "-") {
            $chemin = substr($chemin, 1);
        }

        //Remplacement des '-' par des '/' pour des opérations le chemin réel

        $chemin = str_replace('-', '/', $chemin);

        $filesystem = new Filesystem();

        //On récupère la liste des dossiers et fichiers à afficher sur la page. Les noms seront comparés avec ceux des éléments créés/uploadés

        $finder = new Finder();

        $folders = $finder->in($this->getParameter('upload_dossiers') . '/' . $chemin)->depth(0);


        //Contruction et traitement du formulaire d'envoi de fichier

        $uploadfile = $this->get('form.factory')->createNamedBuilder('uploader')
            ->add('item', FileType::class, array('label' => ' ', 'required' => true,))
            ->getForm();

        $uploadfile->handleRequest($request);

        if ($uploadfile->isSubmitted() && $uploadfile->isValid()) {

            $file = $uploadfile->getData()['item'];

            $fileName = $file->getClientOriginalName();

            foreach ($folders as $presentName) {
                if ($presentName->getFileName() === $fileName) {
                    $fileName = $fileName . "_copie";
                    $this->addFlash('erreur', 'Un fichier du même nom existe déjà. Le nom de votre fichier a donc été modifié.');
                }
            }

            try {
                $file->move(
                    $this->getParameter('upload_dossiers') . '/' . $chemin, $fileName
                );

                $this->addFlash("notif", "Fichier envoyé");
            } catch (FileException $e) {

                die('Erreur lors du déplacement du fichier');

            }

        }


        //Contruction et traitement du formulaire de création de dossier

        $createFolder = $this->get('form.factory')->createNamedBuilder('makedir')
            ->add('nomDossier', null, ['label' => 'Nouveau dossier', 'required' => true])
            ->getForm();

        $createFolder->handleRequest($request);

        if ($createFolder->isSubmitted() && $createFolder->isValid()) {
            $newFolder = $createFolder->getData();


            //Pour éviter les espaces dans l'url, et des noms avec des '-' qui causeraient des erreurs dans le moteur, on les remplace par des '_'

            $folderName = str_replace([' ', '-'], '_', $newFolder['nomDossier']);

            foreach ($folders as $presentName) {
                if ($presentName->getFileName() === $folderName) {
                    $this->addFlash('erreur', 'Un dossier du même nom existe déjà.');
                    $chemin = str_replace('/', '-', $chemin);

                    return $this->redirectToRoute('index', ['chemin' => '-' . $chemin]);
                }
            }


            $filesystem->mkdir($this->getParameter('upload_dossiers') . '/' . $chemin . '/' . $folderName);

            $this->addFlash("notif", "Dossier créé");
        }

        //Renommage d'un dossier

        $renameFolder = $this->get('form.factory')->createNamedBuilder('renamedir')
            ->add('nomDossier', null, ['label' => 'Nouveau dossier', 'required' => true])
            ->add('oldName', null, ['required' => true])
            ->add('chemin', null, ['required' => true])
            ->getForm();

        $renameFolder->handleRequest($request);

        if ($renameFolder->isSubmitted()) {
            $newFolderName = $renameFolder->getData();


            //Pour éviter les espaces dans l'url, et des noms avec des '-' qui causeraient des erreurs dans le moteur, on les remplace par des '_'

            $folderName = str_replace([' ', '-'], '_', $newFolderName['nomDossier']);

            foreach ($folders as $presentName) {
                if ($presentName->getFileName() === $folderName) {
                    $this->addFlash('erreur', 'Un dossier du même nom existe déjà à cet emplacement.');
                    $chemin = str_replace('/', '-', $chemin);

                    return $this->redirectToRoute('index', ['chemin' => '-' . $chemin]);
                }
            }

            sleep(1);
            rename($this->getParameter('upload_dossiers') . '/' . $chemin . '/' . $newFolderName['oldName'], $this->getParameter('upload_dossiers') . '/' . $chemin . '/' . $folderName);

            $this->addFlash("notif", "Le dossier a été rennomé");
        }


        //Décomposition du chemin pour créer les différents éléments du fil d'ariane

        $arianePath = $chemin;

        $arianeParts = explode('/', $arianePath);

        $ariane = [];

        $line = '';

        for ($i = 0; $i < count($arianeParts); $i++) {
            $line = $line . '-' . $arianeParts[$i];

            $ariane[] = $line;
        }

        //Maintenant, il faut remplacer les '/' par des '-' pour pouvoir utiliser le chemin dans la Route

        $chemin = str_replace('/', '-', $chemin);

        return $this->render('/index.html.twig', [
            'chemin' => $chemin,
            'arianeParts' => $arianeParts,
            'ariane' => $ariane,
            'folders' => $folders,
            'makedir' => $createFolder->createView(),
            'uploader' => $uploadfile->createView(),
        ]);
    }

    /**
     * @Route("sup_file/{chemin}/{target}", name="delete_file")
     */

    public function delete_file($chemin, $target)
    {

        $chemin = str_replace('-', '/', $chemin);

        $date = date("Y_m_d_H_i_s_");

        try {

            rename($this->getParameter('upload_dossiers') . '/' . $chemin . '/' . $target, $this->getParameter('upload_dossiers') . '/../TrashCan/' . $date . $target);
            $this->addFlash("notif", "Fichier supprimé");

        } catch (\Exception $e) {

            $this->addFlash('erreur', 'Impossible de supprimer le fichier. Il n\'existait sans doute déjà plus (ce n\'est donc pas bien grave)');

        }

        $chemin = str_replace('/', '-', $chemin);

        return $this->redirectToRoute('index', ['chemin' => '-' . $chemin]);
    }

    /**
     * @Route("dl_file/{chemin}/{target}", name="dl_file")
     */

    public function dl_file($chemin, $target)
    {


        $chemin = str_replace('-', '/', $chemin);

        $item = $this->getParameter('upload_dossiers') . '/' . $chemin . '/' . $target;

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $target);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($item)); //Absolute URL
        ob_clean();
        flush();

        try {

            readfile($item); //Absolute URL

        } catch (\Exception $e) {
            $this->addFlash('erreur', 'Impossible de télécharger le fichier');
        }

        $chemin = str_replace('/', '-', $chemin);

        return $this->redirectToRoute('index', ['chemin' => '-' . $chemin]);
    }

    /**
     * @Route("sup_folder/{chemin}/{target}", name="delete_folder")
     */

    public function delete_folder($chemin, $target)
    {

        $chemin = str_replace('-', '/', $chemin);

        $date = date("Y_m_d_H_i_s_");


        chmod($this->getParameter('upload_dossiers') . '/../TrashCan/', 0777);
        rename($this->getParameter('upload_dossiers') . '/' . $chemin . '/' . $target, $this->getParameter('upload_dossiers') . '/../TrashCan/' . $date . $target);

        $this->addFlash("notif", "Dossier supprimé");


        $chemin = str_replace('/', '-', $chemin);

        return $this->redirectToRoute('index', ['chemin' => '-' . $chemin]);
    }

    /**
     * @Route("rename_folder/{chemin}/{target}", name="rename_folder")
     */

    public function rename_folder(Request $request, $chemin, $target)
    {


        $chemin = str_replace('/', '-', $chemin);

        return $this->redirectToRoute('index', ['chemin' => '-' . $chemin]);
    }

    /**
     * @Route("Create_compta/{chemin}", name="arbo_compta")
     */

    public function arbo_compta($chemin)
    {

        $filesystem = new Filesystem();

        $arboCompta = [
            '01 Relevés Banque',
            '02 Factures Client',
            '03 Factures Fournisseur',
            '04 Cotisations Gérant',
            '05 Cotisations Salariés',
            '06 Bulletins de Salaires',
            '07 TVA',
            '08 Divers'
        ];

        $chemin = str_replace('-', '/', $chemin);

        foreach ($arboCompta as $folder) {

            $folderName = str_replace(' ', '_', $folder);

            $filesystem->mkdir($this->getParameter('upload_dossiers') . '/' . $chemin . '/' . $folderName);
        }

        $this->addFlash("notif", "Les dossiers pour la compta ont été créés !");

        $chemin = str_replace('/', '-', $chemin);

        return $this->redirectToRoute('index', ['chemin' => '-' . $chemin]);
    }

    /**
     * @Route("Create_months/{chemin}", name="arbo_mois")
     */

    public function arbo_mois($chemin)
    {

        $filesystem = new Filesystem();

        $arboCompta = [
            '01 Janvier',
            '02 Février',
            '03 Mars',
            '04 Avril',
            '05 Mai',
            '06 Juin',
            '07 Juillet',
            '08 Août',
            '09 Septembre',
            '10 Octobre',
            '11 Novembre',
            '12 Décembre'
        ];

        $chemin = str_replace('-', '/', $chemin);

        foreach ($arboCompta as $folder) {

            $folderName = str_replace(' ', '_', $folder);

            $filesystem->mkdir($this->getParameter('upload_dossiers') . '/' . $chemin . '/' . $folderName);
        }

        $this->addFlash("notif", "Les dossiers pour les mois de l'année ont été créés !");

        $chemin = str_replace('/', '-', $chemin);

        return $this->redirectToRoute('index', ['chemin' => '-' . $chemin]);
    }

}
<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Abstractor extends AbstractController
{
    /**
     * @Route("/")
     */

    public function redirectionHome()
    {

        return $this->redirect('/Welcome/FactoryCloud');
    }
}
window.addEventListener('load', function() {
    let input = document.getElementById( 'uploader_item' );
        let label = document.querySelectorAll('#uploader label')[0],
            labelVal = label.innerHTML;

        input.addEventListener( 'change', function( e )
        {
            let fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                label.innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });

    const sups = document.getElementsByClassName('folder_suppresser');
    const renameFolder = document.getElementsByClassName('rename_folder');

    const supsFiles = document.getElementsByClassName('file_suppresser');

    if (sups) {
        for (let i = 0; i < sups.length; i++){

            let selec = i+1;
            let associatedFolder = document.querySelector(".foldertype\:nth-of-type("+ selec +")");

            sups[i].addEventListener('click', function (e) {
                let conf = confirm("Supprimer définitivement ce dossier et tout ce qu'il contient ?");

                if (conf === false){
                    e.preventDefault()
                } else{
                    associatedFolder.style.opacity = "0"
                }

            })
        }

    }

    if (renameFolder) {
        for (let i = 0; i < renameFolder.length; i++){

            let selec = i+1;
            let associatedFolder = document.querySelector(".foldertype\:nth-of-type("+ selec +")");

            renameFolder[i].addEventListener('click', function (e) {

                e.preventDefault();


                console.log(associatedFolder);
                let bak = associatedFolder.innerHTML;
                let oldName = associatedFolder.dataset.currentname;
                let chemin = "";

                associatedFolder.innerHTML = "<form id=\"renamedir\" name=\"renamedir\" method=\"post\">\n" +
                    "            <input type=\"text\" class=\"renaming\" name=\"renamedir[nomDossier]\" required=\"required\" placeholder=\""+oldName+"|\">\n" +
                    "            <input type=\"hidden\" name=\"oldname\"  required=\"required\" value=\""+oldName+"\">\n" +
                    "            <input type=\"hidden\" name=\"chemin\"  required=\"required\" value=\"\">\n" +
                    "            <input id=\"rename\"type=\"submit\" value=\"\">\n" +
                    "            </form>"



            })
        }

    }

    if (supsFiles) {
        for (let i = 0; i < supsFiles.length; i++){

            let selec = i+1;
            let associatedFile = document.querySelector(".filetype\:nth-of-type("+ selec +")");

            supsFiles[i].addEventListener('click', function (e) {
                let conf = confirm("Supprimer définitivement ce fichier ?");

                if (conf === false){
                    e.preventDefault()
                } else{
                    associatedFile.style.opacity = "0"
                }

            })
        }

    }

    const popup = document.getElementById('notification');
    let checker = document.querySelector('#notification p');

    if (checker != null) {
        popup.style.display = "block";
        popup.style.animation = "appear 5s ease-in-out 1"
    }

});